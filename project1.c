#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#define N 10000

float rng(){ //return random number between 0 and 1 when called
float tmprn;
tmprn = rand() % N;
return tmprn / N;
}

int main(int argc, char *argv[])
{
int L;
float prob;
if (argc == 3){ //if there is an argument
        L=atoi(argv[1]); //set the argument to L
	prob= 1- atof(argv[2]);
} else {
        L=30; //else use a default value
	prob=0.5;
}

/*srand(27);*/
srand(time(NULL));
int i,reps=100;
float total=0,sqtotal=0,totalsq=0,stdev,av;
for(i=0;i<reps;++i){
int h=0,hopnum=0;
while (h<L) //hopper loop
{
	hopnum++;
	if (rng()>=prob) //if num<0.5
	{
		h++;//hop on
	}
}

sqtotal = sqtotal + (hopnum * hopnum);//calculating stdev
total = total + hopnum;
}

/*calculate std dev*/
totalsq = total * total;
stdev = sqrt( ( sqtotal - ( totalsq / reps ) ) / ( reps - 1 ) );

av = total / reps;
//print results
printf("average is %f\nthe standard deviation is %f\n", av, stdev);

return 0;
}

