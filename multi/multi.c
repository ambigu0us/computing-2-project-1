#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#define N 10000

float rng(){ //return random number between 0 and 1 when called
float tmprn;
tmprn = rand() % N;
return tmprn / N;
}

int main(int argc, char *argv[])
{


/*FILE *file;
file = fopen("data.txt","a+");*/
srand(2115);
srand(time(NULL));

//Command line argument parsing:
//if there are no args, use default row length and repetitions
//else, expect args in the form ./proj1 [row length] [repetitions] [probability]
//todo, proper arg handling
int NumOfPos,reps;
float prob;
if (argc == 4){ //if there are 3 args
	NumOfPos=atoi(argv[1]);
	reps=atoi(argv[2]);
	prob=atof(argv[3]);
} else if (argc != 1) { //if there is a different number of args
	printf("unexpected number of args\n\nUse: ./proj1 [row length] [repetitions] [probability]\n\nOr no args for the default values\n");
	return 1;
} else {
	NumOfPos=30;
	reps=1000;
	prob=0.5;
}
printf("Rep(%d)\tPostion\t\tTotal\n",NumOfPos);
int x[NumOfPos],HopNoArray[NumOfPos];
float gtotalsq=0,stdevhoppers,av,gtotal=0,squaregtotal=0,HopNoTotal=0,HopNoTotalSq=0,finishCount=0,HopNoSqTotal=0,stdevhops,avhops;
int i,z,n,total;


for (n=0;n<NumOfPos;n++){ /*loop to zero the arrays (should be an easier way to do this)*/
x[n]=0;
HopNoArray[n]=0;
}
x[0]=1; //fill the first seat
for (i=0;i<reps;i++){ //main loop
	if ( x[0]==0 ) { //at the beginning of each hop, fill the first seat if it is empty 
		x[0]=1;
	}
	total=0;
	printf("%d:\t",i+1);
	for (z=0;z<NumOfPos;z++){ //print out array for the hops diagrams
	if (x[z]==1)
	{
		printf("%d",x[z]);
	} else {
		printf("0"); //select empty seat character
	}
	total= total + x[z];
	}
	printf("\t%d\n", total);
	gtotal = gtotal + total;
	squaregtotal = squaregtotal + ( total * total );


	for(n=NumOfPos;n>-1;n--){ //move backwards over the row, checking each seat for hopping.
		if (x[n]==1)
		{
			if (rng()<=prob)
		        {
				if (x[n+1]==0) //if the hopper can and wants to hop:
				{
					x[n]=0; //empty seat
					x[n+1]=1; //fill next seat
					HopNoArray[n+1] = HopNoArray[n] + 1; //move the hop number record to the next seat in the array
					HopNoArray[n] = 0; 
				}
				if (n==NumOfPos-1) 
				/*if the last pos is full then it will not move on as
 * 				x[n+1]=NaN. this if statement checks for that case and
 * 				clears it if should move on*/
				{
				x[n]=0;
				//printf("\n\nHopper got to the end in %d hops\n\n", HopNoArray[n] + 1);
				finishCount = finishCount + 1; //if hopper finishes, +1 to finish count
				HopNoTotal = HopNoTotal + HopNoArray[n] + 1; //for stats calculations
				HopNoSqTotal = HopNoSqTotal + ( ( HopNoArray[n] + 1 ) * ( HopNoArray[n] + 1 ) );
				}
	       		} else {
				HopNoArray[n] = HopNoArray[n] + 1;
			}
	
		}
	}
}


/*calculate standard deviation for number of hoppers on the line*/

gtotalsq = gtotal * gtotal; 

stdevhoppers = sqrt( ( squaregtotal - ( gtotalsq / reps ) ) / ( reps - 1 ) );

av = gtotal / reps;

/*calculate standard deviation and average for the number of hops to reach the end*/

HopNoTotalSq = HopNoTotal * HopNoTotal; 

stdevhops = sqrt( ( HopNoSqTotal - ( HopNoTotalSq / finishCount ) ) / ( finishCount - 1 ) );

avhops = HopNoTotal / finishCount;

//print results
printf("\n\nRow length:\t%d\nHops:\t\t%d\nProbability:\t%f",NumOfPos,reps,prob);
printf("\n\nAverage hoppers per line:\t%f\nStandard deviation:\t\t%f\n", av, stdevhoppers);
printf("\nHoppers finished:\t\t%f\nAverage hops per hopper\t\t%f\nStandard deviation:\t\t%f\n", finishCount, avhops, stdevhops);

printf("%f	%f	%f\n",prob,avhops, stdevhops);

return 0;
}

